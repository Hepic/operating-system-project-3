#include <cstdio>
#include "process.h"

Process::Process()
{
    id = arrival_time = exec_time = -1;
    service_time = priority = up_time = -1;
}

Process::Process(const Process &process)
{
    set_process(process.id, process.arrival_time, process.exec_time, process.priority); 
    service_time = process.service_time;
    up_time = process.up_time;
}

Process::Process(int id, int arv, int exc, int pr)
{
    set_process(id, arv, exc, pr); 
    service_time = up_time = -1;
}

void Process::set_process(int id, int arv, int exc, int pr)
{
    this->id = id;
    this->arrival_time = arv;
    this->exec_time = exc;
    this->priority = pr;
}

void Process::set_service_up_time(int srv, int up)
{
    service_time = srv;
    up_time = up;

    if(up_time > exec_time)
        up_time = exec_time;    
}

void Process::reduce_exec_time(int tme)
{
    exec_time -= tme;
}

void Process::print_process() const
{
    printf("id = %d\narrival_time = %d\n", id, arrival_time);
    printf("exec_time = %d\nservice_time = %d\n", exec_time, service_time);
    printf("priority = %d\nup_time = %d\n", priority, up_time);
}

int Process::get_id() const
{
    return id;
}

int Process::get_arrival_time() const
{
    return arrival_time;
}

int Process::get_service_time() const
{
    return service_time;
}

int Process::get_up_time() const
{
    return up_time;
}

int Process::get_exec_time() const
{
    return exec_time;
}

int Process::get_priority() const
{
    return priority;
}
