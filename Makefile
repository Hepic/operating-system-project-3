CC = g++
FLAGS = -g -c
OUT = run
OBJS = main.o help_functions.o Tsemaphores.o list.o node.o process.o

run: $(OBJS)
	$(CC) -g $(OBJS) -o $(OUT)

main.o: main.cpp
	$(CC) $(FLAGS) main.cpp

help_functions.o: help_functions.cpp
	$(CC) $(FLAGS) help_functions.cpp

Tsemaphores.o: Tsemaphores.cpp
	$(CC) $(FLAGS) Tsemaphores.cpp

list.o: list.cpp
	$(CC) $(FLAGS) list.cpp

node.o: node.cpp
	$(CC) $(FLAGS) node.cpp

process.o: process.cpp
	$(CC) $(FLAGS) process.cpp

clean:
	rm -f $(OBJS) $(OUT) 
