#include <cstdio>
#include "node.h"

Node::Node()
{
    process = NULL;
    next = NULL; 
}

Node::Node(const Node &node)
{
    this->process = node.process;
    this->next = node.next;
}

Node::Node(const Node *node)
{
    this->process = node->process;
    this->next = node->next;
}

Node::Node(Process process)
{
    this->process = new Process(process); 
    this->next = NULL;
}

void Node::set_next(Node *next)
{
    this->next = next;
}

Node* Node::get_next() const
{
    return next;
}

Process* Node::get_process() const
{
    return process;
}

void Node::print_node() const
{
    process->print_process();
}
