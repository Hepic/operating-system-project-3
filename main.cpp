#include <cstdio>
#include <cstdlib>
#include <ctime> 
#include "help_functions.h"
#include "Tsemaphores.h"
#include "list.h"
#include "node.h"
#include "process.h"

int PROCESS_NUM, SEM_NUM;
double k = 0.5;
double lambda_arv = 0.2, lambda_exc = 0.2;
double lambda_up = 0.2;

int finished_process;
List processes;
List *semaphores;


int main(int argc, char *argv[])
{
    srand(time(NULL));
    
    const char *Pcs_str = read_argument(argv, argc, "-P");
    const char *Sem_str = read_argument(argv, argc, "-S");
    const char *k_str = read_argument(argv, argc, "-k");
    const char *larv_str = read_argument(argv, argc, "-lA");
    const char *lexc_str = read_argument(argv, argc, "-lE");
    const char *lup_str = read_argument(argv, argc, "-lU");

    PROCESS_NUM = convert_str_to_int(Pcs_str);
    SEM_NUM = convert_str_to_int(Sem_str);
    k = convert_str_to_double(k_str);
    lambda_arv = convert_str_to_double(larv_str);
    lambda_exc = convert_str_to_double(lexc_str);
    lambda_up = convert_str_to_double(lup_str);
    
    semaphores = new List[SEM_NUM];

    int sem_id = create_semaphores(SEM_NUM);
    initialize_semaphores(sem_id, SEM_NUM);
    
    printf("Processes to simulate: \n--------------------------------\n");
    
    for(int i=0; i<PROCESS_NUM; ++i)
    {
        int arv = exponential_distribution(lambda_arv);
        int exc = exponential_distribution(lambda_exc);
        int pr = uniform_distribution(1, 7);
        
        Process process(i, arv, exc, pr);
        Node node(process); 
        processes.push(node);
        printf("id = %d, arv = %d, exec = %d, priority = %d\n", i, arv, exc, pr);
    }

    printf("--------------------------------\n");
    
    for(int step=1; finished_process<PROCESS_NUM; ++step)
    {   
        printf("------------step %d------------\n", step);
        
        // Up Process
        for(int i=0; i<SEM_NUM; ++i)
        {
            Process *process = semaphores[i].get_owner();

            if(process != NULL)
            {
                int pcs_id = process->get_id();
                int up_time = process->get_up_time();
                int srv_time = process->get_service_time();
                int diff = step - srv_time;
                
                printf("\nSemaphore=%d -> id=%d, srv_time=%d, up_time=%d\n", i, pcs_id, srv_time, up_time);

                if(diff == up_time)
                {
                    process->reduce_exec_time(up_time);
                    process->set_service_up_time(-1, -1);

                    int exc = process->get_exec_time();                    

                    if(exc > 0)
                    { 
                        Node node(*process); 
                        processes.push(node);
                        printf("Push id=%d to processes with exec_time=%d\n", pcs_id, exc);
                    }
                    else
                    {
                        int arv_time = process->get_arrival_time();
                        int prior = process->get_priority();
                        //int wait_time = step - arv_time - 
                        //printf("prior=%d, wait_time=", prior, arv_time, srv_time, step);
                        printf("Process %d finished\n", pcs_id);
                        ++finished_process;
                    }

                    delete process;
                    process = NULL;

                    sem_up(sem_id, i);

                    if(!semaphores[i].is_empty())
                    {
                        Node *node = semaphores[i].front(); 
                        process = node->get_process();
                        pcs_id = process->get_id();
                        
                        printf("Process %d is now owner of semaphore=%d\n", pcs_id, i);
                        sem_down(sem_id, i);
                        
                        int up_time = exponential_distribution(lambda_up);
                        process->set_service_up_time(step, up_time);
                        
                        semaphores[i].set_owner(process);
                        semaphores[i].pop();

                        delete process;
                        process = NULL;
                    }
                    else
                        semaphores[i].set_owner(NULL);
                }
            }
        }
        
        // Down Process
        Node *node = processes.front();

        while(node != NULL)
        {
            Node *next = node->get_next();
            Process *process = node->get_process();

            if(process->get_arrival_time() <= step)
            {
                printf("\n");
                process->print_process();

                bool try_lock = try_with_probability(k);
                
                if(try_lock)
                {
                    int sem_rand = uniform_distribution(0, SEM_NUM-1); 
                    printf("\nPick %d semaphore\n", sem_rand);

                    if(semaphores[sem_rand].get_owner() != NULL)
                    {
                        Node new_node(*process);
                        semaphores[sem_rand].push(new_node);
                        printf("Push in queue\n");
                    }
                    else
                    {
                        sem_down(sem_id, sem_rand);
                        printf("Lock sempahore\n");

                        int up_time = exponential_distribution(lambda_up);
                        process->set_service_up_time(step, up_time);
                        
                        semaphores[sem_rand].set_owner(process);
                    }
                    
                    processes.remove(process);

                    delete process;
                    process = NULL;
                }
            }

            node = next;
        }

        printf("\n\n");
    }

    delete_semaphores(sem_id);
    delete[] semaphores;
    
    return 0;
}
