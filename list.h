#ifndef List_H
#define List_H

#include "node.h"

class List
{
    Node *head, *tail;
    Process *owner;

    public:
        List();
        void push(Node);
        void pop();
        Node* front() const;
        void set_owner(Process*);
        void remove(const Process*);
        void print_list() const;
        Process *get_owner() const;
        bool is_empty() const;
};

#endif
