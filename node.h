#ifndef NODE_H
#define NODE_H

#include "process.h"

class Node
{
    Process *process;
    Node *next;

    public:
        Node(); 
        Node(const Node &node);
        Node(const Node*);
        Node(Process process);
        void set_next(Node*);
        Node* get_next() const;
        Process* get_process() const;
        void print_node() const;
};

#endif
