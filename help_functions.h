#ifndef HELP_FUNCTIONS_H
#define HELP_FUNCTIONS_H

#include "process.h"

int uniform_distribution(int, int);
int exponential_distribution(double);
bool try_with_probability(double);
const char* read_argument(char*[], int, const char*);
int convert_str_to_int(const char*);
double convert_str_to_double(const char*);
void perror_exit(const char*);

#endif
