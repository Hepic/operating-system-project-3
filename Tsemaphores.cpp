#include <cstdio>
#include "Tsemaphores.h"
#include "help_functions.h"

int create_semaphores(int val)
{
    int sem_id;

    if((sem_id = semget(IPC_PRIVATE, val, 0666 | IPC_CREAT)) < 0)
        perror_exit("semget");

    return sem_id;
}

void initialize_semaphores(int sem_id, int val)
{
    ushort *semvals = new ushort[val];

    for(int i=0; i<val; ++i)
        semvals[i] = 1;

    union semnum arg;
    arg.array = semvals;

    if(semctl(sem_id, 0, SETALL, arg) < 0)
        perror_exit("semctl");
    
    delete[] semvals;
}

void set_semaphore(int sem_id, int sem_num, int val)
{
    union semnum arg;
    arg.val = val;

    if(semctl(sem_id, sem_num, SETVAL, arg) < 0)
        perror_exit("semctl");
}

void sem_up(int sem_id, int sem_num)
{
    sembuf oper = {sem_num, 1, 0};

    if(semop(sem_id, &oper, 1) < 0)
        perror_exit("semop");
}

void sem_down(int sem_id, int sem_num)
{
    sembuf oper = {sem_num, -1, 0};

    if(semop(sem_id, &oper, 1) < 0)
        perror_exit("semop");
}

void delete_semaphores(int sem_id)
{
    if(semctl(sem_id, 0, IPC_RMID, 0) < 0)
        perror_exit("semctl");
}
