#ifndef PROCESS_H
#define PROCESS_H

class Process
{
    int id, arrival_time, exec_time;
    int service_time, priority, up_time;
    
    public:
        Process();
        Process(const Process&);
        Process(int, int, int, int);
        void set_process(int, int, int, int);
        void set_service_up_time(int, int);
        void reduce_exec_time(int);
        void print_process() const;
        int get_id() const;
        int get_arrival_time() const;
        int get_service_time() const;
        int get_up_time() const;
        int get_exec_time() const;
        int get_priority() const;
};

#endif
