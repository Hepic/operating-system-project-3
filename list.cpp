#include <cstdio>
#include <cstdlib>
#include "list.h"

List::List()
{
    head = tail = NULL;
    owner = NULL;
}

void List::push(Node node)
{
    Node *new_node = new Node(node); 

    if(tail != NULL)
        tail->set_next(new_node); 

    tail = new_node; 
    
    if(head == NULL)
        head = tail; 
}

void List::pop()
{
    if(head == NULL)
        return;

    Node *next = head->get_next();
    
    if(head == tail)
        tail = next;

    delete head;
    head = next;
}

Node* List::front() const
{
    return head;
}

void List::set_owner(Process *process)
{
    if(process == NULL)
    {
        owner = NULL;
        return;
    }

    owner = new Process(*process);
}

void List::remove(const Process *process)
{
    if(head == NULL)
        return;
    
    if(head->get_process() == process)
    {
        Node *next = head->get_next();
        
        if(head == tail)
            tail = next;
        
        delete head;
        head = next;
        
        return;
    }

    Node *node = head;

    while(node->get_next() != NULL)
    {
        if(process == node->get_next()->get_process()) 
            break;

        node = node->get_next();
    }
    
    Node *next = node->get_next();
    
    if(next == NULL)
        return;

    node->set_next(next->get_next());
    
    if(tail == next)
        tail = node;

    delete next;
    next = NULL;
}

void List::print_list() const
{
    Node *node = head;

    while(node != NULL)
    {
        node->print_node();
        node = node->get_next();
    }
}

Process* List::get_owner() const
{
    return owner;
}

bool List::is_empty() const
{
    return head == NULL;
}
