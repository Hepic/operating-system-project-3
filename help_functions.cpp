#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cstring>
#include "help_functions.h"

int uniform_distribution(int beg, int end)
{
    int diff = end - beg + 1;
    int val = rand() % diff + beg;
    
    return val;
}

int exponential_distribution(double lambda)
{
    double p = rand() / (double)(RAND_MAX);
    int val = -log(1 - p) / lambda;

    return val + 1;
}

bool try_with_probability(double prob)
{
    double val = rand() / (double)RAND_MAX;
    return prob >= val; 
}

const char* read_argument(char *str[], int len, const char *flag)
{
    for(int i=0; i<len; ++i)
    {
        if(!strcmp(str[i], flag))
            return str[i+1];           
    }
}

int convert_str_to_int(const char *str)
{
    int ret = 0;

    for(int i=0; i<strlen(str); ++i)
        ret = ret*10 + str[i] - '0';

    return ret;
}

double convert_str_to_double(const char *str)
{
    double ret = 0, div = 10;
    bool after_dot = false;

    for(int i=0; i<strlen(str); ++i)
    {
        if(str[i] == '.')
        {
            after_dot = true;
            continue;
        }

        if(after_dot)
        {
            ret += (str[i] - '0') / div;
            div *= 10;
        }
        else
            ret = ret*10 + str[i] - '0';
    }

    return ret;
}

void perror_exit(const char *msg)
{
    perror(msg);
    exit(1);
}
