#ifndef TSEMAPHORES_H
#define TSEMAPHORES_H

#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <errno.h>

union semnum {
    int val;
    struct semid_ds *buff;
    unsigned short *array;
};

int create_semaphores(int);
void initialize_semaphores(int, int);
void set_semaphore(int, int, int);
void sem_up(int, int);
void sem_down(int, int);
void delete_semaphores(int);

#endif
